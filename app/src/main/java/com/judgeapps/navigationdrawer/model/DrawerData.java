package com.judgeapps.navigationdrawer.model;

/**
 * Created by Pavan on 28-05-2017.
 */

public class DrawerData{

    private int icon;
    private String title;

    public DrawerData(String title, int icon) {
        this.icon=icon;
        this.title=title;
    }


    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
