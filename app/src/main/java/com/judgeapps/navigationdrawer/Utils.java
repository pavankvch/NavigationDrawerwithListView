package com.judgeapps.navigationdrawer;

import android.app.Activity;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Pavan on 28-05-2017.
 */

class Utils {

    public static void hideSoftKeyboard(Activity base){
        if (base.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) base.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(base.getCurrentFocus().getWindowToken(), 0);
        }

    }
}
