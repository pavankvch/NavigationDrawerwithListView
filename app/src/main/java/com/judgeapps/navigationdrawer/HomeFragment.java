package com.judgeapps.navigationdrawer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Pavan on 28-05-2017.
 */

public class HomeFragment extends Fragment{


    public static Fragment newInstance() {
        HomeFragment homeFragment=new HomeFragment();
        return homeFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.home_fragment, container, false);
        init(view);
        return view;
    }

    private void init(View view) {

    }
}
