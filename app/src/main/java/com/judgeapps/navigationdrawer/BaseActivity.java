package com.judgeapps.navigationdrawer;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.judgeapps.navigationdrawer.model.DrawerData;

import java.util.ArrayList;

/**
 * Created by Pavan on 17-05-2017.
 */

public class BaseActivity extends AppCompatActivity implements View.OnClickListener {

    public BaseActivity base;
    public ImageView imgMenu, imgBack;
    public DrawerLayout drawerLayout;
    public ListView lvDrawer;
    private DrawerAdapter drawerAdapter;
    public ArrayList<DrawerData> drawerList=new ArrayList<>();
    public FragmentManager fragmentManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init(savedInstanceState);
    }

    public void init(Bundle savedInstanceState) {
        base= BaseActivity.this;

        setContentView(R.layout.activity_base);

        imgMenu=(ImageView)findViewById(R.id.img_menu);
        imgBack=(ImageView)findViewById(R.id.imgBack);
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerLayout);
        lvDrawer =(ListView)findViewById(R.id.lvNav);

        fragmentManager=getSupportFragmentManager();

        /*add header in listview*/
        LayoutInflater inflater = getLayoutInflater();
        View listHeader = inflater.inflate(R.layout.row_drawer_header, lvDrawer, false);
        lvDrawer.addHeaderView(listHeader);

        /*Setup drawer list item*/
        drawerAdapter = new DrawerAdapter(getApplicationContext());
        lvDrawer.setAdapter(drawerAdapter);


        setDrawerData();



        imgMenu.setOnClickListener(this);
        imgBack.setOnClickListener(this);


        startFragment(HomeFragment.newInstance());
    }

    private void startFragment(Fragment fragment) {
        this.fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    private void setDrawerData() {
        drawerList.clear();

        String[] mainArray=getResources().getStringArray(R.array.drawerMainArray);


        drawerList.add(new DrawerData(mainArray[0], R.mipmap.ic_launcher_round));
        drawerList.add(new DrawerData(mainArray[1], R.mipmap.ic_launcher_round));
        drawerList.add(new DrawerData(mainArray[2], R.mipmap.ic_launcher_round));
        drawerList.add(new DrawerData(mainArray[3], R.mipmap.ic_launcher_round));
        drawerList.add(new DrawerData(mainArray[4], R.mipmap.ic_launcher_round));

        drawerAdapter.notifyDataSetChanged();

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.img_menu:

                Utils.hideSoftKeyboard(base);
                if (drawerLayout.isDrawerOpen(lvDrawer))
                    drawerLayout.closeDrawer(lvDrawer);
                else
                    drawerLayout.openDrawer(lvDrawer);
                break;

            case R.id.imgBack:

                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (drawerLayout.isDrawerOpen(lvDrawer))
            drawerLayout.closeDrawer(lvDrawer);
        else
            ActivityCompat.finishAffinity(base);

    }

    private class DrawerAdapter extends BaseAdapter{

        private Context context;

        private DrawerAdapter(Context context) {
            this.context=context;
        }

        @Override
        public int getCount() {
            return drawerList.size();
        }

        @Override
        public Object getItem(int position) {
            return drawerList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder viewHolder;

            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(R.layout.drawer_item_layout, parent, false);
                viewHolder.tvLabel=(TextView) convertView.findViewById(R.id.tvLabel);
                viewHolder.imgIcon = (ImageView) convertView.findViewById(R.id.imgIcon);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();

            }


            viewHolder.tvLabel.setText(drawerList.get(position).getTitle());
            viewHolder.imgIcon.setImageResource(drawerList.get(position).getIcon());

            return convertView;
        }

        private class ViewHolder {
            TextView tvLabel;
            ImageView imgIcon;
        }
    }
}
